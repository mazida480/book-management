
# Book Management

This project is done for managing books.


## API Reference

#### Create a user

```http
  POST /api/v1/create-user
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `api_key`      | `string` | **Required**. Sign up as a user. |

#### Login with email and password

```http
  POST /api/v1/login
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `api_key`      | `string` | **Required**. Login for authentication |

#### Get a book with its id

```http
  GET /api/book/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of a book to fetch |

#### Get all books

```http
  GET /api/v1/books
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `api_key` | `string` | **Required**. Your API key |



#### Publish or create a book

```http
  POST /api/v1/publish-book
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `api_key`      | `string` | **Required**. Create or publish a book|

#### Update a book with its id

```http
  PUT /api/v1/update-book/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Update a book with its id. |

#### Delete a book with its id

```http
  DELETE /api/delete-book/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Delete a book|



## Authors

- Abdul Mazid


## Installation

Install project dependencies with npm

```bash
  npm install
```
    
## Documentation
Do the follwing to run this project:
* git clone https://gitlab.com/mazida480/book-management.git
* Go to .env file and update it.
* npm install
* npm start


