const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config()

//Connection to cloud mongoDB.
const db = main().then(_ => {console.log('Connected to database successfully.')}).catch(err => console.log('Database error: ' + err))

async function main(){
    await mongoose.connect(process.env.URI)
}

module.exports = db