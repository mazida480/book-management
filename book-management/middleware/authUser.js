const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')

dotenv.config()


//Middleware to authenticate a user before resirecting to a page.
const authUser = async(req, res, next) => {
    const authHeader = (req.headers.authorization) || ""

    if(!authHeader || !authHeader.startsWith('Bearer')){
        next('Please login.')
    }
    
    const token = authHeader.split(" ")[1]

    try{
        const payload = jwt.verify(token, process.env.JWT_SECRET)

        req.user = {userId:payload.userId}
        next()
    }catch(error){
        next("Authentication require/failed.")
    }

}

module.exports = {authUser}