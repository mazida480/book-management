const User = require('../model/user.js')

const createUser = async (req, res) => {
    const isUser = await User.findOne({email:req.body.email})

    if(isUser){
      res.send({message:'Email or password is already used. Please login.'})
    }else{

      try{

        const user = await new User(req.body)
        user.save()
        res.status(200).send(
          {
            success:true,
            message:'User created',
            name:`${user.firstName} ${user.lastName}`,
            email:user.email
          }
        )

      }catch{

        res.send({error:"Error occured while creating user."})

      }

    }


}

const getUsers = async(req, res) => {
  const users = await User.find()
  res.send({users})
}

const loginController = async (req, res) => {
  const {email, password} = req.body

  if(!email || !password){
    res.status(400).send({message:'Please provide all fields.'})
  }

  const user = await User.findOne({email}).select('+password')

  if(!user){
    res.send({message:'Invalid username or password'})
  }

  const isMatch = await user.comparePassword(password)

  if(!isMatch){
    res.send({message:'Invalid username or password'})
  }

  user.password = undefined
  const token = user.createJWT()
  res.status(200).send(
    {
      success:true,
      message:'Loged in successfully.',
      user,
      token
    }
  )


}

module.exports = {createUser, getUsers, loginController}
