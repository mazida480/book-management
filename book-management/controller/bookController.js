const Book = require('../model/book.js')


//Get a book by its id.
const getBookByIdController = async(req, res) => {
    await Book.findOne({_id:req.params.id}).then(book => {
        res.send({book})
    }).catch(err => res.send({err}))
}


//Get all the books from its database table/collection.
const getBooksController = async(req, res) => {
    try{
        const books = await Book.find()
        console.log(books)
        if(!books.length > 0){
            res.send({message:"There is no book."})
        }else{

            res.status(200).send({books})
        }
    }catch(err){
        res.status(400).send(err)
    }
}

//Create or publish a new book.
const createBookController = async(req, res) => {
    const book = await Book.findOne({title:req.body.title})

    if(!book){
        const book = new Book(req.body)
        await book.save()
        res.status(200).send(
            {
                success:true,
                message:'Book published.',
                book
            }
        )
    }
}

//Update an existing book with an id.
const updateBookController = async(req, res) => {
    const book = await Book.findById(req.params.id)

    book.title = req.body.title
    book.author = req.body.author
    book.publication_year = Date.now()

    await book.save()

    res.send(
        {
            success:true,
            message:'Book updated',
            book
        }
    )
}

//Delete a book with a particular id
const deleteBookByIdController = async(req,res) => {
    console.log(req.params.id)
    const book = await Book.deleteOne({_id:req.params.id})
    console.log(book)

    if(book.deletedCount > 0){
        res.send({
            success:true,
            message:"Book deleted successfully.",
            book
        })
    }else{
        res.status(400).send(
            {
                success:false,
                message:'Could not delete. Try again.'
            }
        )
    }
}

//Search a book by its author name
const searchBookController = async(req, res) => {

    const name = new RegExp(req.query.author, 'i')
    const book = await Book.find({author:name}, 'author').exec()

    if(book){
        res.send(
            {
                success:true,
                message:'We found the book.',
                book
            }
        )
    }else{
        res.send({message:"Not found"})
    }
}

module.exports = {createBookController, getBooksController, updateBookController, getBookByIdController, deleteBookByIdController, searchBookController}

