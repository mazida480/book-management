const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const router = require('./routes/router.js')
const db = require('./config/connectdb.js')


dotenv.config()

app.use(bodyParser.urlencoded({extended:false}))
app.use(cors({origin:'*'}))
app.use(express.json())

app.listen(process.env.PORT, () => {
    console.log(`App is runnint on port ${process.env.PORT}`)
})
let re = new RegExp('john','i')
console.log(re)
app.use('/api/v1', router)
