const express = require('express')
const router = express.Router()
const user_controller = require('../controller/userController.js')
const authorizer = require('../middleware/authUser.js')

//Book controller
const bookController = require('../controller/bookController.js')




//End-point to create or register a user.
router.post('/create-user', user_controller.createUser)

//End-point to list users.
// router.get('/users', user_controller.getUsers)

//End-point to login or authenticate a user.
router.post('/login', user_controller.loginController)


//Router for book related operations.

//End-point to get book by id.
router.get('/book/:id', bookController.getBookByIdController)

//End-point to get all the books.
router.get('/books', bookController.getBooksController)

//End-point to create or publish a book.
router.post('/publish-book', authorizer.authUser,  bookController.createBookController)

//Update a book
router.put('/update-book/:id', bookController.updateBookController)

//delete a book
router.delete('/delete-book/:id', bookController.deleteBookByIdController)

//Search a book by its author
router.get('/search-book', bookController.searchBookController)


module.exports = router