const mongoose = require('mongoose')
const bookSchema = new mongoose.Schema(
    {
       title:{type:String, required:[true, "Title is required."]},
       author:{type:String, required:[true, "Auther name required."]},
       publication_year:{type:Date, default:Date.now()}
    }
)

// bookSchema.pre('save', function(){
//     this.publication_year = new Date().getFullYear()
// })

const Book = mongoose.model('Book', bookSchema)

module.exports = Book