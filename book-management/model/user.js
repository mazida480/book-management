const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')

dotenv.config()

const userSchema = new mongoose.Schema(
    {
        firstName:{type:String, required:[true, 'First name is required.']},

        lastName:{type:String},

        email:{type:String, required:[true, 'Email is required.']},

        password:{type:String, required:[true, 'Password is required.']}

    },
    {timestamps:true}
)

//Middleware
userSchema.pre('save', async function(){
    const salt = await bcrypt.genSalt(10)
    this.password = await bcrypt.hash(this.password, salt)
})

//Compare password
userSchema.methods.comparePassword = async function(userPassword){
    const isMatch = await bcrypt.compare(userPassword, this.password)

    return isMatch
}

//JWT
userSchema.methods.createJWT = function(){
    const token = jwt.sign({userId:this._id}, process.env.JWT_SECRET, {expiresIn:'1d'})

    return token
}


const user = mongoose.model('User', userSchema, 'Users')

module.exports = user